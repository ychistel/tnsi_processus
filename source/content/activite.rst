Qu'est-ce qu'un processus ?
===========================

Lorsqu'on exécute un programme, le système d'exploitation crée un processus qui lui permet de gérer son exécution, de lui attribuer les ressources nécéssaires comme la mémoire et d'éviter les conflits avec les autres programmes exécutés en même temps.

Le système d'exploitation dispose d'un programme qui affiche les différents processus. Pour windows, il s'agit du **gestionnaire des tâches**.

#.  Ouvrir le gestionnaire des tâches de windows. Vous devez obtenir la fenêtre suivante:

    .. figure:: ../img/taskmgr_1.png
        :align: center

    Assurez-vous d'être sur l'onglet **Processus** et d'avoir les mêmes champs d'informations, à savoir, le **nom**, le **type**, le **PID**, le **nom du processus** et la **ligne de commande**. 

    .. note::

        Si les champs ne correspondent pas, faites un clic droit sur la ligne des champs et sélectionner les champs souhaités.

        .. figure:: ../img/taskmgr_2.png
            :align: center

#.  Retrouver dans le gestionnaire des tâches:

    a.  Le nom du processus associé au gestionnaire des taches ?
    b.  Le programme et le chemin absolu associé au gestionnaire des tâches ?

#.  Le **PID** est l'identifiant d'un processus.

    a.  Quel est l'identifiant du processus associé au gestionnaire des tâches ?
    b.  Fermer le gestionnaire des tâches, puis l'ouvrir à nouveau. Quel est son PID ? 
    Quelle conclusion en tirer.

#.  Pour chaque programme exécuté, un processus est créé.

    a.  Ouvrir le bloc-notes de windows et relever son PID.
    b.  Est-il possible d'exécuter plusieurs fois le bloc-notes. Si oui, quelle est l'information qui différencie les processus ?
    
#.  On crée un fichier texte avec le bloc-notes nommé ``test.txt`` contenant le texte "les processus".

    a.  Est-il possible d'ouvrir ce fichier texte avec un autre éditeur de texte ? De le modifier ?
    b.  Est-il possible d'ouvrir le fichier ``test.txt`` avec le bloc-notes et un autre éditeur de texte en même temps. De les modifier en même-temps ?
    c.  Quel problème peut se poser si on accède à une même ressource avec 2 processus différents ?

#.  Ouvrir le fichier ``test.txt`` avec l'application **LibreOffice**.

    a.  Ouvrir l'explorateur de fichier et accéder au dossier contenant le fichier ``test.txt``.
    b.  Déplacer ce fichier dans un autre dossier. Que se passe-t-il ? Comment l'expliquer ? 

#.  Le navigateur que vous utilisez a généré un processus visible dans le gestionnaire des tâches.

    a.  Que remarquez-vous sur ce processus ? Comment l'expliquer ?
    b.  Quel est le PID du processus associé à votre navigateur ? Comment le distinguer des autres processus ?
    c.  Est-il possible de mettre fin à tous ces processus ?
