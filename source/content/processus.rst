Les processus
=============

Un ordinateur est construit selon le modèle d'architecture **Von Neumann**.

.. image:: ../img/von_neumann.svg
	:align: center
	:width: 420
	:class: margin-bottom-16

D'après le modèle de Von Neumann :

-   les instructions du programme sont placées dans la mémoire vive;
-   le processeur exécute les différentes instructions du programme.

Une machine possède un seul processeur qui exécute une seule instruction à la fois. Lorsque plusieurs programmes sont exécutés en même temps, le processeur exécute les instructions de ces différents programmes les unes après les autres. 

.. note::
	
	Aujourd'hui, les processeurs :
	
	-   sont capables d'exécuter plusieurs millions d'instruction par seconde.
	-	possèdent plusieurs coeurs qui permettent d'exécuter plusieurs programmes en parallèle, mais il n'y a pas autant de coeurs que de programmes à exécuter !

De nombreuses questions se posent. 

-   Comment un unique processeur peut-il exécuter plusieurs programmes sans erreur ? 
-   Comment un processeur exécute-t-il les instructions des différents programmes ? 
-   Comment un processeur peut-il associer une instruction exécutée au bon programme ? 
-   Comment un programme peut-il utiliser une ressource de la machine ? 
-   Quel programme doit-être exécuté en priorité ?

La réponse à ces questions est unique : le **processus**.

Un programme, un processus
----------------------------

Chaque programme exécuté par le processeur est associé à un **processus**. Le système d'exploitation gère les processus pour que les programmes soit exécutés par le processeur sans erreur.

Un processus est identifié par un numéro unique : le **PID** acronyme de **Process Identifier**. 

Un programme peut lui-même lancer l'exécution d'un autre programme créant ainsi un nouveau processus. Ce deuxième processus est le fils du premier processus. Ce nouveau processus possède son **PID** et aussi un **PPID** acronyme de **Parent Process Identifier** qui est le **PID** du processus père.

Ordonnancement
--------------

Plusieurs programmes et donc plusieurs processus sont actifs simultanément. Le système d'exploitation partage le processeur entre tous les processus: c'est le rôle de **l'ordonnanceur**.

**L'ordonnanceur** partage le processeur entre les différents processus. Chaque processus se voit allouer un temps d'exécution pendant lequel il est à l'état **élu**. Ensuite le processus passe à l'état **prêt** et ainsi de suite. On dit que c'est un ordonnancement **préemptif**.

- Le passage d'un processus de l'état **prêt** à l'état **élu** est appelé **election**.
- Le passage de l'état **élu** à l'état **prêt** est appelé **préemption**.  

.. image:: ../img/election_preemption.svg
	:align: center
	:alt: election_preemption.svg
	:width: 360
	:class: margin-bottom-16

Lorsque plusieurs processus sont créés, un seul est **élu**, les autres sont à l'état **prêt** ou à l'état **bloqué**. L'ordonnanceur, après un cycle d'exécution (quantum de temps), met le processus **élu** à l'état **prêt** et passe un autre processus de l'état **prêt** à **élu**.

On représente les états des processus par un chronogramme.

.. image:: ../img/chronogramme_1.svg
	:align: center
	:alt: chronogramme_1.svg
	:width: 360
	:class: margin-bottom-16

Cycle de vie
---------------------

Un processus suit un cycle de vie défini par 3 états différents :

-   l'état **prêt** qui signifie que le programme est placé dans une file avec tous les autres programmes à exécuter.
-   l'état **élu** qui signifie que le programme est en cours d'exécution.
-   l'état **bloqué** qui signifie que le programme est en attente d'une ressource non disponible et utilisée par un autre processus.

.. image:: ../img/etats_processus.svg
	:alt: etats-processus.svg
	:align: center
	:class: margin-bottom-16
	
Lorque l'exécution d'un programme est lancée, un **processus** est créé et celui-ci est à l'état **prêt**. Ensuite, le processus se trouve dans l'un des trois états qui constitue son cycle de vie. 
À la fin de l'exécution du programme, le processus passe à l'état **terminé** indiquant la fin du programme.

Interblocage
------------

La plupart des ressources matérielles ne peuvent être utilisées que par un seul processus à la fois. Les systèmes d'exploitation ont des mécanismes pour contrôler l'utilisation des ressources par un seul processus. Un mécanisme répandu est l'utilsation d'un **verrou**.

-   un processus demande un **verrou** pour utiliser une ressource.
-   un processus relâche le **verrou** pour libérer la ressource.

Un processus qui demande un **verrou** sur une ressource est propriétaire du verrou et seul lui peut libérer la ressource en relâchant le verrou.

L\'**interblocage** est une situation dans laquelle les processus sont bloqués et ce sur un temps infini. 

Un **interblocage** peut se produire lorsque des processus font appel à des ressources déjà utilisées par d'autres processus. Lorsqu'une ressource n'est pas disponible, le processus passe à l'état bloqué. Si aucune des ressources n'est libérée, les processus restent à l'état bloqué et créent un interblocage.

.. admonition:: Exemple

	Supposons 2 processus ayant besoin de deux ressources matérielles:

	-  Le processus 1 demande la ressource 1 avec un verrou
	-  Le processus 2 demande la ressource 2 avec un verrou
	-  Le processus 1 demande la ressource 2 avec un verrou; il passe à l'état bloqué car la ressource n'est pas disponible.
	-  Le processus 2 demande la ressource 1 avec un verrou; il passe à l'état bloqué car la ressource n'est pas disponible.

	Les deux processus restent à l'état bloqué. Il y a situation d'interblocage.

Les systèmes d'exploitation utilisent des verrous et des algorithmes d'ordonnancement pour éviter ces situations d'interblocage.  

.. note::

	On peut citer deux types de verrous:

	-   le **mutex** est un verrou qui n'autorise l'utilisation d'une ressource que par un seul processus à la fois.
	-   le **sémaphore** est un verrou qui autorise l'utilisation d'une ressource par un nombre fini de processus.
