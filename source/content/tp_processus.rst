TP : Deux tortues en parallèles
================================

Le module ``turtle`` permet de créer des dessins géométriques en utilisant des fonctions de tracés.

Voici 3 fonctions de ce module:

-   ``forward(d)`` qui trace un sement de longueur ``d``
-   ``left(a)`` qui pivote la tortue à gauche d'un angle ``a`` degrés.
-   ``done()`` qui met fin à la session de dessin.

Premiers tracés
-----------------

#.  Écrire une fonction ``carre(d)`` avec le paramètre ``d``  qui trace un carré de côté ``d``.
#.  Écrire un programme qui trace un carré de côté 100.
#.  Vérifier qu'un processus associé à votre programme est bien créé.
#.  Modifier votre programme pour tracer 2 carrés (non superposés).
#.  Combien de processus sont créés pour tracer ces 2 carrés.
#.  Est-il possible de tracer les 2 carrés en même temps ?

Tracer 2 carrés
----------------

Pour réaliser les 2 carrés en même temps, on utilise des **thread**. Un processeur contient des threads qui permet de réaliser des instructions en parallèle dans un même processus.

On donne le programme suivant:

.. code:: python

    import turtle
    from threading import Thread

    # Création de la première tortue
    tortue1 = turtle.Turtle()
    tortue1.color("red")  # Couleur de la tortue 1 : rouge
    tortue1.shape("turtle")  # Forme de la tortue 1 : tortue classique

    # Création de la deuxième tortue
    ...

    def carre(tortue,distance):
        for i in range(4):
            tortue.forward(distance)
            tortue.left(90)

    if __name__ == '__main__':
        # Création d'un premier thread
        t1 = Thread(name="tortue1",target=carre,args=(tortue1,100))
        
        # Création d'un deuxième thread
        ...

        # lancement du premier thread
        t1.start()

        # lancement du deuxième thread
        ...

        turtle.done()

#.  Copier et coller le programme ci-dessus dans un nouveau fichier python.
#.  Créer une seconde tortue de couleur bleue.
#.  Compléter le programme en créant un second **thread** associé à la seconde tortue.
#.  Exécuter le programme et vérifier que les deux carrés sont tracés en même temps.
#.  Combien de processus sont associés à ce programme ? Relever les PID.

Deux carrés, deux processus
----------------------------

Le langage Python dispose d'un module ``multiprocessing`` qui permet de créer des processus différents dans un même programme. On va dans cette partie créer 2 carrés dans 2 processus différents.

Voici le programme complet :

.. code:: python

    import multiprocessing
    import turtle
    import time

    # Fonction pour le premier processus de dessin
    def draw_square():
        window = turtle.Screen()
        window.title("Tortue 1 - Carré")
        window.setup(width=400, height=400)  # Taille de la fenêtre
        window._root.geometry("400x400+0+0")  # Position : en haut à gauche
        my_turtle = turtle.Turtle()

        for _ in range(4):
            my_turtle.forward(100)
            my_turtle.right(90)

        time.sleep(5)  # Garde la fenêtre ouverte 2 secondes
        window.bye()   # Ferme la fenêtre

    # Fonction pour le second processus de dessin
    def draw_circle():
        window = turtle.Screen()
        window.title("Tortue 2 - Cercle")
        window.setup(width=400, height=400)  # Taille de la fenêtre
        window._root.geometry("400x400+420+0")  # Position : à droite de la première fenêtre
        my_turtle = turtle.Turtle()

        my_turtle.circle(100)

        time.sleep(5)  # Garde la fenêtre ouverte 2 secondes
        window.bye()   # Ferme la fenêtre

    if __name__ == "__main__":
        # Création des processus
        p1 = multiprocessing.Process(target=draw_square)
        p2 = multiprocessing.Process(target=draw_circle)

        # Démarrage des processus
        p1.start()
        p2.start()

        # Attendre la fin des deux processus
        p1.join()
        p2.join()

        print("Les deux dessins sont terminés")

#.  Copier et coller le code de ce programme dans un nouveau fichier python.
#.  Exécuter ce programme et relever les PID des différents processus.
#.  Ajouter un troisième processus qui trace un triangle.