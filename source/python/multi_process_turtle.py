import multiprocessing
import turtle
import time

# Fonction pour le premier processus de dessin
def draw_square():
    window = turtle.Screen()
    window.title("Tortue 1 - Carré")
    window.setup(width=400, height=400)  # Taille de la fenêtre
    window._root.geometry("400x400+0+0")  # Position : en haut à gauche
    my_turtle = turtle.Turtle()

    for _ in range(4):
        my_turtle.forward(100)
        my_turtle.right(90)

    time.sleep(5)  # Garde la fenêtre ouverte 2 secondes
    window.bye()   # Ferme la fenêtre

# Fonction pour le second processus de dessin
def draw_circle():
    window = turtle.Screen()
    window.title("Tortue 2 - Cercle")
    window.setup(width=400, height=400)  # Taille de la fenêtre
    window._root.geometry("400x400+420+0")  # Position : à droite de la première fenêtre
    my_turtle = turtle.Turtle()

    my_turtle.circle(100)

    time.sleep(5)  # Garde la fenêtre ouverte 2 secondes
    window.bye()   # Ferme la fenêtre

if __name__ == "__main__":
    # Création des processus
    p1 = multiprocessing.Process(target=draw_square)
    p2 = multiprocessing.Process(target=draw_circle)

    # Démarrage des processus
    p1.start()
    p2.start()
    
    # Attendre la fin des deux processus
    p1.join()
    p2.join()

    print("Les deux dessins sont terminés")
