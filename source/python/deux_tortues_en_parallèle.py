import turtle
from threading import Thread

# Création de la première tortue
tortue1 = turtle.Turtle()
tortue1.color("red")  # Couleur de la tortue 1 : rouge
tortue1.shape("turtle")  # Forme de la tortue 1 : tortue classique

# Création de la deuxième tortue
tortue2 = turtle.Turtle()
tortue2.color("blue")
tortue2.shape("turtle")

def carre(tortue,distance):
    for i in range(4):
        tortue.forward(distance)
        tortue.left(90)

if __name__ == '__main__':
    # Création d'un premier thread
    t1 = Thread(name="tortue1",target=carre,args=(tortue1,100))
    
    # Création d'un deuxième thread
    t2 = Thread(name="tortue2",target=carre,args=(tortue2,-100))

    # lancement du premier thread
    t1.start()

    # lancement du deuxième thread
    t2.start()

    turtle.done()
