from multiprocessing import Process,Queue

def worker1(q):
    q.put("Hello from process 1!")

def worker2(q):
    q.put("Hello from process 2!")

if __name__ == "__main__":
    q = Queue()
    p1 = Process(target=worker1, args=(q,))
    p2 = Process(target=worker2, args=(q,))
    p1.start()
    p2.start()
    print(q.get())  # Output: Hello from process 1!
    print(q.get())  # Output: Hello from process 2!
    p1.join()
    p2.join()