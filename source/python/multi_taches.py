from threading import Thread
from time import time, ctime, sleep
from queue import Queue

class Worker(Thread):
    
    def __init__(self,queue,name,delay):
        print(f'Création du Worker {name}')
        self.queue = queue
        self.delay = delay
        self.job_ended = False
        Thread.__init__(self,name=name)
        
    def run(self):
        for i in range(5):
            if self.job_ended:
                print(f'Arrêt forcé de {self.getName()}')
                self.queue.task_done()
                return
            print(f'Appel {self.getName()}, {i}, {ctime(time())}')
            sleep(self.delay)
        print(f'Arrêt naturel de {self.getName()}')
        self.queue.task_done()
        
class Stopper(Thread):
    
    def __init__(self, threads,delay):
        print('Création du stoppeur')
        self.delay = delay
        self.threads = threads
        Thread.__init__(self)
        
    def run(self):
        sleep(self.delay)
        print("Demande d'arrêt des tâches")
        for t in self.threads:
            t.job_ended = True
            
try:
    q = Queue(3)
    t1 = Worker(q,"T1",1)
    t2 = Worker(q,"T2",2)
    t3 = Worker(q,"T3",3)
    t1.start()
    t2.start()
    t3.start()
    q.put(t1)
    q.put(t2)
    q.put(t3)
    s = Stopper((t1,t2,t3),6)
    s.start()
    print('Attente de la fin des tâches')
    q.join()
    print('Tâches terminées, reprise du flux principal')
except:
    print("Erreur : unable to start thread")
                