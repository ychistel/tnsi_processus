import multiprocessing
import time

# Fonctions définies au niveau global
def process_1():
    for i in range(5):
        print(f"Processus 1 - itération {i}")
        time.sleep(1)

def process_2():
    for i in range(5):
        print(f"Processus 2 - itération {i}")
        time.sleep(1.5)

# Vérifie que le code est bien exécuté en tant que script principal
if __name__ == "__main__":
    # Création des processus
    p1 = multiprocessing.Process(target=process_1)
    p2 = multiprocessing.Process(target=process_2)

    # Démarrage des processus
    p1.start()
    p2.start()

    # Attendre que les processus se terminent
    p1.join()
    p2.join()

    print("Les deux processus sont terminés")
