Les processus
===============

.. image:: img/intro_processus.jpeg
    :align: center
    :width: 100%
    :class: margin-bottom-16

Un système d'exploitation est un ensemble de programmes qui permet à un utilisateur d'exécuter d'autres programmes en utilisant les ressources de la machine. Le système d'exploitation associe à chaque programme en cours d'exécution un **processus**.

Dans cette partie, on s'intéresse à la gestion des processus et des ressources par un système d'exploitation.

-   décrire la création d'un processus, l'ordonnancement de plusieurs processus par un système d'exploitation.
-   mettre en évidence le risque d'interblocage (deadlock).

.. toctree::
    :maxdepth: 1
    :hidden:
    
    content/activite.rst
    content/processus.rst
    content/exercice.rst
    content/tp_processus.rst
    content/tp_os_processus.rst
    content/os_processus.rst
